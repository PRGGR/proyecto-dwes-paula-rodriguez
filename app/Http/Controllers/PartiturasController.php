<?php

namespace App\Http\Controllers;

use App\Partitura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;

class PartiturasController extends Controller
{
    public function getTodas(){
		
		$partituras = Partitura::all();
	    return view('partituras.index', 
	    	array('arrayPartituras' => $partituras));
	}


	public function getVer($id){

		$partitura = Partitura::findOrFail($id);
	    return view('partituras.mostrar', 
	    	array('partitura' => $partitura));
	}

	public function getCrear(){
	    return view('partituras.crear');
	}

	public function postCrear(Request $request){
		
		$partitura = new Partitura();

		$partitura->user_id = Auth::user()->id;
		$partitura->nombre = $request->nombre;
		$partitura->epoca = $request->epoca;
		$partitura->autor = $request->autor;
		$partitura->descripcion = $request->descripcion;
		$partitura->imagen = $request->imagen->store('','partituras');

		try {
			$partitura->save();
			return redirect('partituras')->with('mensaje', "Creado con éxito");
		} catch (Exception $ex){
			return redirect('partituras')->with('mensaje', "Fallo al crear la partitura");
		}

	}

}
