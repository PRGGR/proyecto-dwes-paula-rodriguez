<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// INICIO
Route::get('/', 'InicioController@getInicio');

// MOSTRAR TODAS PARTITURAS
Route::get('partituras', 'PartiturasController@getTodas');


// MOSTRAR PARTITURAS
Route::get('partituras/ver/{id}', 
	'PartiturasController@getVer')->where('id', '[0-9]+');

Route::get('partituras/crear', 'PartiturasController@getCrear')->middleware('auth');

// CREAR POST
Route::post('partituras/crear/', 'PartiturasController@postCrear')->middleware('auth');



// // EDITAR POST

// Route::post('mascotas/editar/{id}', 'MascotasController@postEditar');

// Route::group(['middleware' => 'auth'], function(){
// 	Route::get('partituras/crear', 'PartiturasController@getCrear');
// 	Route::get('mascotas/editar/{id}', 'MascotasController@getEditar')->where('id', '[0-9]+');
// });

