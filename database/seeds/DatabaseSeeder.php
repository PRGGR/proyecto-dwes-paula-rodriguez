<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Partitura;

class DatabaseSeeder extends Seeder
{
	private $arrayPartituras = array(
		array(
			'nombre' => 'Partitura 1',
			'epoca' => 'Renacimiento',
			'autor' => 'Monteverdi',
			'descripcion' => 'Descripción 1',
			'imagen' => 'partitura1.jpg'
		),
		array(
			'nombre' => 'Partitura 2',
			'epoca' => 'Barroco',
			'autor' => 'Bach',
			'descripcion' => 'Descripción 2',
			'imagen' => 'partitura2.jpg'
		),
		array(
			'nombre' => 'Partitura 3',
			'epoca' => 'Clasicismo',
			'autor' => 'Mozart',
			'descripcion' => 'Descripción 3',
			'imagen' => 'partitura3.jpg'
		),
	);

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){

        self::seedUsers();
 		$this->command->info('Tabla usuarios inicializada con datos');

 		self::seedPartituras();
 		$this->command->info('Tabla partituras inicializada con datos');
    }

    private function seedUsers(){

    	DB::table('users')->delete();

    	$u = new User();
    	$u->name = "Paula";
    	$u->email = "paularodriguez451@gmail.com";
    	$u->password = bcrypt("usuario@1");
		$u->save();
		
    }

    private function seedPartituras(){

    	DB::table('partituras')->delete();

    	foreach ($this->arrayPartituras as $partitura){
			$p = new Partitura();
			$p->user_id = User::all()->first()->id;
			$p->nombre = $partitura['nombre'];
			$p->epoca = $partitura['epoca'];
			$p->autor = $partitura['autor'];
			$p->descripcion = $partitura['descripcion'];
			$p->imagen = $partitura['imagen'];
			$p->save();
		}
    }

}
