@extends('layouts.master')
@section('titulo')
	Mostrar
@endsection
@section('contenido')
	<div class="row">
		<div class="col-sm-2" style="margin-left: 50px">
			<img src="{{asset('assets/imagenes/')}}/{{$partitura->imagen}}" style="height:300px"/>
		</div>
		
		<div class="col-sm-9">
			<div style="margin-left: 30px; margin-top: 20px;">
				<h4 style="min-height:45px;margin:5px 0 10px 0">
					{{$partitura->nombre}} - {{$partitura->autor}}</h4>
				<p style="font-size: 12px;">{{$partitura->descripcion}}</p>
			</div>
		</div>
	</div>
@endsection