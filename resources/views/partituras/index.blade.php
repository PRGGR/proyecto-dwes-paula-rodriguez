@extends('layouts.master')
@section('titulo')
	Partituras
@endsection
@section('contenido')
	<div class="row">
		@foreach($arrayPartituras as $partitura)
			<div class="col-xs-12 col-sm-6 col-md-3" style="text-align: center;">
				<a href="{{ url('/partituras/ver/' . $partitura->id ) }}">
					<img src="{{asset('assets/imagenes/')}}/{{$partitura->imagen}}" style="height:200px" class="rounded mx-auto d-block img-thumbnail"/>
				</a>
				<h3 style="min-height:45px;margin:5px 0 10px 0; text-align: center;">
					{{$partitura->nombre}}
				</h3>
				<h5 style="font-style: italic">{{$partitura->autor}}</h5>
				<a href="{{ url('/partituras/ver/' . $partitura->id ) }}" style="font-size: 12px;">(Leer +)</a>									
			</div>
		@endforeach
	</div>

@endsection