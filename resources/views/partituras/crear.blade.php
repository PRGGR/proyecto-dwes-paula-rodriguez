@extends('layouts.master')
@section('titulo')
	Crear
@endsection
@section('contenido')
	<div class="row">
		<div class="offset-md-3 col-md-6">
			<div class="card">
				<div class="card-header text-center">
				 Añadir partitura
				</div>
				<div class="card-body" style="padding:30px">
					<form action="{{ action('PartiturasController@postCrear') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" id="nombre" class="form-control">
						</div>
						<div class="form-group">
							<label for="epoca">Época</label>
							<input type="text" name="epoca" id="epoca" class="form-control">
						</div>
						<div class="form-group">
							<label for="autor">Autor</label>
							<input type="text" name="autor" id="autor" class="form-control">
						</div>
						<div class="form-group">
							<label for="descripcion">Descripción</label>
							<input type="text" name="descripcion" id="descripcion" class="form-control">
						</div>
						<div class="form-group">
							<label for="imagen">Partitura PDF</label>
							<input type="file" name="imagen" id="imagen" class="form-control">
						</div>
						<div class="form-group text-center">
							<input type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;" value="Añadir partitura">
						</div>
					</form>
				</div>
			</div>
		 </div>
	</div>
@endsection